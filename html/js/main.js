// logging function


function trackerDebug(log_txt) {

    if (window.console != undefined) {
        console.log(log_txt);
    }
	
}

// Query String Grabber

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results == null) return "";
    else return decodeURIComponent(results[1].replace(/\+/g, " "));
}


function getInternetExplorerVersion()
// Returns the version of Internet Explorer or a -1
// (indicating the use of another browser).
{
    var rv = -1; // Return value assumes failure.
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null) rv = parseFloat(RegExp.$1);
    }
    return rv;
}

var ieVersion = getInternetExplorerVersion()

var jsWidth = 800;
var jsHeight = 600;



var ratio = 1280 / 720;

var minW = 800;
var minH = minW / ratio;

//***************************************************************************
// JQuery Ready Start
//***************************************************************************
$(document).ready(function () {

	// setUpClickEvents()
	if ($.browser.msie == true) {

		$("html").css({
			"overflow-y": "auto"
		});

	}

    // define resize function
    $(window).resize(function (event) {
		
		var wWin = $(window).width ();
		var hWin = $(window).height ();
		
		var rWin = wWin / hWin;
		
		trackerDebug (rWin + " : " + ratio);
		
		var w = minW;
		var h = minH;
		
		if (rWin >= ratio) {
		
			// match the width dimension
			trackerDebug ("matching width");
			
			w = Math.max (wWin, minW);
			if(w == wWin) {
				w = "100%";
				$("#main").css('width', '100%');
				w = $('#main').width();
			}
			else {
				$("#main").css('width', Math.floor(w));
			}
			h = w / ratio;
			$("#main").css('height', Math.floor(h));
			
		} else {
			// match the height dimension
			trackerDebug ("matching height");
			
			h = Math.max (hWin, minH);
			if(h == hWin) {
				h = "100%";
				$("#main").css('height', '100%');
				h = $('#main').height();
			}
			else {
				$("#main").css('height', Math.floor(h));
			}
			w = h * ratio;
			$("#main").css('width', Math.floor(w));
		}
		
		var hw = w * 0.5;
		var hh = h * 0.5;

		$(document).scrollLeft (20);
		
		/*
        var heightCalc = Math.max (jsHeight, $(window).height());
        var widthCalc =  Math.max (jsWidth, $(window).width());

        $("#main").css({
		
            "height": heightCalc

        });			

        trackerDebug("height " + heightCalc + "  " + " window width " + widthCalc + " main width " + $("#main").width() + " scroll " + document.getElementById('main').scrollWidth)

		// <= could be a problem here but seems to fix IE 6
        if (widthCalc <= jsWidth) {

            //$("#main").css("overflow-x", "auto");	
			
            $("#main").css ({
                "width": widthCalc
            });

        } else {

            $("#main").css({
                "width": "100%"
            }); 

        }
		*/
		
		wWin = $(window).width ();
		
		var scrollx = (w - wWin) * 0.5;
		
		$(document).scrollLeft (scrollx);
    });

    $(window).resize();

})

//***************************************************************************
// JQuery Ready END
//***************************************************************************

//***************************************************************************
// SWFObject Embed
//***************************************************************************
//swfobject callback

function embeddCallBack() {

    trackerDebug("swfembedded")

    $(window).resize();

}

var random_KEY = Math.floor(Math.random() * 1001)

//Dynamic publishing with swfObject 
var flashvars = {};
flashvars.lc_key = random_KEY;

var params = {};
params.allowScriptAccess = "always";
params.bgcolor = "#000000";
params.wmode = "window";
var attributes = {};
attributes.name = "flashcontent";
attributes.id = "flashcontent";
attributes.play = "true";
attributes.menu = "false";
attributes.scale = "noscale";
attributes.wmode = "window";
attributes.bgcolor = "#000000";
attributes.allowfullscreen = "true";
attributes.allowscriptaccess = "always";
attributes.allownetworking = "all";
attributes.width = "100%";
attributes.height = "100%";

//A 'name' attribute with the same value as the 'id' is REQUIRED for Chrome/Mozilla browsers
swfobject.embedSWF("swfs/os_exploding.swf", "flashcontent", "100%", "100%", "10.1", null, flashvars, params, attributes, embeddCallBack);
